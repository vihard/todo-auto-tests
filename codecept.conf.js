const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

let hdls
!!process.env.CI ? hdls = false : hdls = true

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  tests: './src/*-tests.js',
  output: './output',
  helpers: {
    Playwright: {
      browser: 'chromium',
      url: 'https://todomvc.com/examples/react/dist/',
      show: hdls,
      waitForNavigation: "networkidle0"
    }
  },
  include: {
    I: './steps_file.js'
  },
  plugins: {
    allure: {
      enabled: true,
      require: 'allure-codeceptjs',
      outputDir: './output/allure-raw',
    }
  },
  name: 'codecept'
}