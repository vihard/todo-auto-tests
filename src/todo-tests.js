Feature('Todo Tests');

Scenario('Add a new todo item', ({ I }) => {
  const todoName = 'New Todo Item'
  I.amOnPage('/');
  I.see('todos', "header[class='header'] h1");
  I.fillField('#todo-input', todoName);
  I.pressKey('Enter');
  I.see(todoName);
});

Scenario('Mark a todo item as completed', ({ I }) => {
  const todoName = 'Todo to be completed'
  I.amOnPage('/');
  I.fillField('#todo-input', todoName);
  I.pressKey('Enter');
  I.click('.toggle');
  // I.seeElement(`//label[contains(text(),'${todoName}')]//ancestor::li[@class="completed"]`);    //correct string without error
  I.seeElement(`//label[contains(text(),'${todoName}p')]//ancestor::li[@class="completed"]`); //here is a simulated error to demonstrate how the report works
});

Scenario('Delete a todo item', ({ I }) => {
  const todoName = 'Todo to be deleted'
  I.amOnPage('/');
  I.fillField('.new-todo', todoName);
  I.pressKey('Enter');
  I.moveCursorTo('.todo-list li');
  I.click('.todo-list li button.destroy');
  I.dontSee(todoName);
});

Scenario('Edit an existing todo item', ({ I }) => {
  const todoName = 'Todo for editing'
  const editedName = 'Edited todo'
  const editSelector = "//div[@class='view']//input[@id='todo-input']"
  I.amOnPage('/');
  I.fillField('.new-todo', todoName);
  I.pressKey('Enter');
  I.doubleClick(`//label[contains(text(),'${todoName}')]`);
  I.clearField(editSelector)
  I.fillField(editSelector, editedName);
  I.pressKey('Enter');
  I.dontSee(todoName);
  I.see(editedName);
});