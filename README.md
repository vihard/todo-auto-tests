# todo-auto-tests

This repo contains tests for TodoMVC application https://todomvc.com/examples/react/dist/. 
This requires Node.js v14+ to run.

## Installation

Initialize the local repository and clone the project

```
git clone https://gitlab.com/safmar-api/todo-auto-tests.git
```

Go to cloned project folder and install packages

```
cd todo-auto-tests
npm i
```

## Running tests

To run all tests just simply type

```
npm run test
```

### Please note
In the test `Mark a todo item as completed`, an error was intentionally made to demonstrate the operation of the report.

## Reporting

The project provides the ability to create an `Allure report`. 
To generate a report after the tests are completed, use the command
```
npm run genreport
```
If the report is generated successfully, you should see a message
```
Report successfully generated to .\output\allure-report
```
To view the report use the command
```
npm run openreport
```
The report will be opened as an HTML page in the default browser

If you need to convert the `Allure report` into a separate HTML page for forwarding, you can use the following command
```
npm run htmlpatchreport
```
If the conversion is successful, you will see a message like this which indicates the name of the file and its location
```
Done
> Saving result as ./output/allure-report\complete.html
Done. Complete file size is: 7225987 bytes. It took 0.2727782000303268s.
```

